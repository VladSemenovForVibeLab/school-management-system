package com.semenov.SCHOOLManagementSystemSpring.enums;

public enum UserRole {
    ADMIN,
    STUDENT
}
