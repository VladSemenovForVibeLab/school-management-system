package com.semenov.SCHOOLManagementSystemSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolManagementSystemSpringApplication {
	public static void main(String[] args) {
		SpringApplication.run(SchoolManagementSystemSpringApplication.class, args);
	}
}
